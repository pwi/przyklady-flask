# -*- coding: utf-8 -*-

from flask import Flask

app = Flask(__name__)

@app.route('/profile/<username>')
def show_profile(username):
    return u'Nazywam się %s' % username

@app.route('/div/<float:val>/')
def divide(val):
    return '%0.2f podzielone przez 2 daje %0.2f' % (val, val / 2)

@app.route('/blog/entry/<int:id>/', methods=['GET'])
def read_entry(id):
    return 'czytanie wpisu %d' % id

@app.route('/blog/entry/<int:id>/', methods=['POST'])
def write_entry(id):
    return 'stworzenie wpisu %d' % id


if __name__ == '__main__':
    app.run(debug=True)